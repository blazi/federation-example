// This file will not be regenerated automatically.
//
// It serves as dependency injection for your app, add any dependencies you require here.
package graph

import "go.mongodb.org/mongo-driver/mongo"

type Resolver struct{
	Test int
	Client *mongo.Client
}
