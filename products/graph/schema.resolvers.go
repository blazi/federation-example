package graph

// This file will be automatically regenerated based on the schema, any resolver implementations
// will be copied through when generating and any unknown code will be moved to the end.

import (
	"context"
	generated1 "federation/products/graph/generated"
	"federation/products/graph/model"
	"go.mongodb.org/mongo-driver/bson"
	"log"
)

func (r *queryResolver) TopProducts(ctx context.Context, first *int) ([]*model.Product, error) {

	coll := r.Client.Database("testing").Collection("products")


	cursor, err := coll.Find(ctx, bson.M{})
	if err != nil {
		log.Fatal(err)
	}

	defer cursor.Close(ctx)


	var data []*model.Product
	if err = cursor.All(ctx, &data); err != nil {
		log.Fatal(err)
	}

	return data, nil
}

// Query returns generated1.QueryResolver implementation.
func (r *Resolver) Query() generated1.QueryResolver { return &queryResolver{r} }

type queryResolver struct{ *Resolver }
