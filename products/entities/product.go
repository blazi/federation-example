package entities

type Product2 struct {
	Upc   string `json:"upc" bson:"upc,omitempty"`
	Name  string `json:"name" bson:"name,omitempty"`
	Price int    `json:"price" bson:"price,omitempty"`
}

func (Product2) IsEntity() {}
